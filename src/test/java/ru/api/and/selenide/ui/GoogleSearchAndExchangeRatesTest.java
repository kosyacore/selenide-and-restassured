package ru.api.and.selenide.ui;

import com.codeborne.selenide.Configuration;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ru.api.and.selenide.pages.BankPage;
import ru.api.and.selenide.pages.GoogleMainPage;
import ru.api.and.selenide.pages.GoogleSearchResultsPage;

import static com.codeborne.selenide.Selenide.open;
import static org.testng.Assert.assertTrue;

public class GoogleSearchAndExchangeRatesTest {

    @BeforeTest
    public void setUp() {
        Configuration.startMaximized = true;
        System.setProperty("selenide.browser", "chrome");
        System.setProperty("webdriver.chrome.driver", "path/to/chrome");
    }

    @Test
    public void googleSearchAndExchangeRatesTest() {
        String baseUrl = "https://google.com";
        String siteToSearch = "Открытие";
        String siteToOpen = "Банк «Открытие»";

        open(baseUrl);

        new GoogleMainPage().search(siteToSearch);
        new GoogleSearchResultsPage().openSite(siteToOpen);

        assertTrue(BankPage.getUSDSellExchangeRate() > BankPage.getUSDBuyExchangeRate());
        assertTrue(BankPage.getEURSellExchangeRate() > BankPage.getEURBuyExchangeRate());
    }
}
