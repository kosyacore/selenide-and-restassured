package ru.api.and.selenide.api;

import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.api.and.selenide.objects.Users;

import static io.restassured.RestAssured.get;
import static org.testng.Assert.assertNotNull;

public class GetUsersTest extends BaseTest {

    @Test
    public void getUsersTest() {
        Users usersList = get(getApiUri() + "/users?page=2")
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .and()
                .contentType(ContentType.JSON)
                .extract()
                .as(Users.class);

        assertNotNull(usersList.getPage());
        assertNotNull(usersList.getPerPage());
        assertNotNull(usersList.getTotal());
        assertNotNull(usersList.getTotalPages());

        usersList.getData()
                .forEach(Assert::assertNotNull);
    }
}
