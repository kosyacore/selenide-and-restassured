package ru.api.and.selenide.api;

import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import ru.api.and.selenide.objects.User;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class CreateUserTest extends BaseTest {

    @Test
    public void createUserTest() {
        final Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("name", "Steve");
        requestBody.put("job", "Apple");

        User user = given()
                .header("Content-Type", "application/json")
                .body(requestBody)
                .when()
                .post(getApiUri() + "/users")
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_CREATED)
                .extract()
                .body()
                .as(User.class);

        assertEquals(requestBody.get("name"), user.getName());
        assertEquals(requestBody.get("job"), user.getJob());
        assertNotNull(user.getId());
        assertNotNull(user.getCreatedAt());
    }
}
