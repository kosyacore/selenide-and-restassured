package ru.api.and.selenide.api;

class BaseTest {
    private String apiUri = "https://reqres.in/api";

    public String getApiUri() {
        return apiUri;
    }
}
