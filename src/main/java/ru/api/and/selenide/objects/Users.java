package ru.api.and.selenide.objects;

import com.fasterxml.jackson.annotation.JsonAlias;

import java.util.HashMap;
import java.util.List;

public class Users {
    private String page;

    @JsonAlias("per_page")
    private String perPage;
    private String total;

    @JsonAlias("total_pages")
    private String totalPages;

    private List<HashMap<String, Object>> data;

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getPerPage() {
        return perPage;
    }

    public void setPerPage(String perPage) {
        this.perPage = perPage;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(String totalPages) {
        this.totalPages = totalPages;
    }

    public List<HashMap<String, Object>> getData() {
        return data;
    }

    public void setData(List<HashMap<String, Object>> data) {
        this.data = data;
    }
}
