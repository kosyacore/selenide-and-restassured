package ru.api.and.selenide.pages;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class GoogleMainPage {

    public GoogleMainPage search(String text) {
        $(By.name("q"))
                .shouldBe(Condition.visible)
                .shouldBe(Condition.focused)
                .setValue(text)
                .pressEnter();
        return this;
    }
}
