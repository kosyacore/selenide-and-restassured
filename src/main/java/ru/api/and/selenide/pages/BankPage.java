package ru.api.and.selenide.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$$;

public class BankPage {

    private static ElementsCollection exchangeRates;

    static {
        exchangeRates = getExchangeRates();
    }

    private static ElementsCollection getExchangeRates() {
        return $$(By.xpath("//span[@class='main-page-exchange__rate']"));
    }

    public static double getUSDBuyExchangeRate() {
        return Double.parseDouble(exchangeRates
                .get(0)
                .scrollTo()
                .shouldBe(Condition.visible)
                .text()
                .replace(",", "."));
    }

    public static double getUSDSellExchangeRate() {
        return Double.parseDouble(exchangeRates
                .get(1)
                .scrollTo()
                .shouldBe(Condition.visible)
                .text()
                .replace(",", "."));
    }

    public static double getEURBuyExchangeRate() {
        return Double.parseDouble(exchangeRates
                .get(2)
                .scrollTo()
                .shouldBe(Condition.visible)
                .text()
                .replace(",", "."));
    }

    public static double getEURSellExchangeRate() {
        return Double.parseDouble(exchangeRates
                .get(3)
                .scrollTo()
                .shouldBe(Condition.visible)
                .text()
                .replace(",", "."));
    }
}
