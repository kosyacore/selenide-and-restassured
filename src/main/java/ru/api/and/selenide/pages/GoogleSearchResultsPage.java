package ru.api.and.selenide.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$$;

public class GoogleSearchResultsPage {

    private ElementsCollection getResults() {
        return $$(By.className("r"));
    }

    public GoogleSearchResultsPage openSite(String siteToOpen) {
        getResults().find(text(siteToOpen))
                .shouldBe(Condition.visible)
                .click();
        return this;
    }
}
